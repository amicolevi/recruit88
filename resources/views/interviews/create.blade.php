@extends('layouts.app')

@section('title', 'Create interview')

@section('content')
        <h1>Create interview</h1>
        <form method = "post" action = "{{action('InterviewsController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "date">Date</label>
            <input type = "date" class="form-control" name = "date">
        </div>     
        <div class="form-group">
            <label for = "review">Review</label>
            <input type = "text" class="form-control" name = "review">
        </div> 
      
        @foreach ($interviews as $interview)
        <form method="POST" action="{{ route('interviews.store',[$interview->id]) }}">
        @endforeach     
                    @csrf  
                    <div class="form-group row">
                   
                    <div class="col-md-6">
                        <select class="form-control" name="candidate_id">                                                                         
                        
                          @foreach ($candidates as $candidate)
                          <option value="{{ $candidate->id }}"> 
                              {{ $candidate->name }} 
                          </option>
                          @endforeach 
                           
                        </select>
                    </div>
                    <input name="id" type="hidden" value = {{$candidate->id}} >
                                    
                </form> 

                <tr>
                @foreach ($interviews as $interview)
        <form method="POST" action="{{ route('interviews.store',[$interview->id]) }}">
        @endforeach     
                    @csrf  
                    <div class="form-group row">
                   
                    <div class="col-md-6">
                        <select class="form-control" name="user_id">                                                                         
                        
                          @foreach ($users as $user)
                          <option value="{{ $user->id }}"> 
                              {{ $user->name }} 
                          </option>
                          @endforeach 
                           
                        </select>
                    </div>
                    <input name="id" type="hidden" value = {{$user->id}} >
                    <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
    
                            </div>
                    </div>                    
                </form> 
                </tr>
                </tr> 
           <td>Created</td><td>{{$interview->created_at}}</td>
        </tr>
        <tr>
           <td>Updated</td><td>{{$interview->updated_at}}</td>  
        </tr>  
                <div>
            <input type = "submit" name = "submit" value = "Create candidate">
        </div> 
                </div>
 
@endsection
