@extends('layouts.app')

@section('title', 'Interviews')

@section('content')
<!-- @if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif -->
<h1>Candidate interview</h1>
<table class = "table table-dark">
    <!-- the table data -->
    @foreach($interviews as $interview)
        <tr>
            <td>Id</td><td>{{$interview->id}}</td>
        </tr>
        <tr>
            <td>Date</td><td>{{$interview->date}}</td>
        </tr>
        <tr>
            <td>Review</td><td>{{$interview->review}}</td>
        </tr> 
        <tr>
            <td>Candidate</td><td>{{$interview->candidate_id}}</td>
        </tr> 
           <td>Created</td><td>{{$interview->created_at}}</td>
        </tr>
        <tr>
           <td>Updated</td><td>{{$interview->updated_at}}</td>  
        </tr>  
        @endforeach  
        </table>
@endsection
