<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interviews extends Model
{
    protected $fillable = ['date','review', 'candidate_id', 'user_id'];

    public function candidates()
    {
        return $this->belongsTo('App\Candidate');
    } 

    public function users()
    {
        return $this->belongsTo('App\User');
    } 
}
