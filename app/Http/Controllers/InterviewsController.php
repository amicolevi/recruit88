<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interviews;
use App\Candidate;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class InterviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function myInterviews()
    {        
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $interviews = $user->interviews;
        //$candidates = Candidate::all();
        $users = User::all();
        // $statuses = Status::all();        
        return view('interviews.show', compact('interviews','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('assign-user');
        Gate::authorize('add-interview');
        $users = User::all();
        $candidates = Candidate::all();
        $interviews = Interviews::all();
        return view('interviews.create', compact('interviews','candidates', 'users'));
    }

    // public function selectcandidate(Request $request){
    //     $iid = $request->id;
    //     $cid = $request->candidate_id;
    //     $interviews = Interviews::findOrFail($iid);
    //     $interviews->save();
    //     return redirect('candidates');
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $interview = new Interviews();
        $inter = $interview->create($request->all());
        $inter->candidate_id = $request->candidate_id; 
        $inter->user_id = $request->user_id;
        $inter->save();
        return redirect('candidates');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $candidates = Candidate::all();
        $interviews = Interviews::all();
        return view('interviews.show', compact('interviews', 'candidates'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
