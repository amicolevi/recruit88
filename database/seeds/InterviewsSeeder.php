<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
                'date' => '2020-07-15',
                'review' => 'It was a good one, we hope to continew',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'date' => '2020-06-20',
                'review' => 'We think that his the right man',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                            
            ]);
    }
}
